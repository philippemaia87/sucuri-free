'''
Numerical Integration - Sucuri Version
Leandro Marzulo <leandro.marzulo@flatlabs.com.br>

Usage:
python NumericalIntegration.py <use_cluster> <number_of_tasks> <number_of_workers_per_node> <number_of_points> <computation_weight>

For shared memory systems the number of tasks and workers are tipically the same. For clusters, you can set the number of workers to match the number of cores in each cluster node, while the number of tasks is the total amount of parallel tasks desired.
'''
import sys, os

#need this environment variable if Sucuri is installed manually.
sys.path.append(os.environ['SUCURIHOME'])
from sucuri import *
import math

def intNum(args):
	peso=args[0]
	start=args[1]
	end=args[2]
	s = 0
	for x in xrange(start, end+1):
		total = 0
	        itTotal = (((x % 10) * peso) + 1)
       		for y in xrange(1, itTotal):
                	total += math.sqrt(y)
		s+= total
	return s

def intNumTotal(args):
	total = 0.0
	for partial in args:
		total+= partial
	print 'Result: %f' %total


ntasks = int(sys.argv[2])
nworkers = int(sys.argv[3])
n = int(sys.argv[4])
peso = int(sys.argv[5])

graph = DFGraph()
sched = Scheduler(graph, nworkers, mpi_enabled = (sys.argv[1]=='True'))

R = Node(intNumTotal, ntasks)
graph.add(R)

fpeso=Feeder(peso)
graph.add(fpeso)

size = n / ntasks

for i in range(ntasks):

	start = (i * size) + 1
	if(i == ntasks - 1):
                end = n
        else:
		end = (i + 1) * size

	fstart=Feeder(start)
	fend=Feeder(end)	
	graph.add(fstart)
	graph.add(fend)
	
	#print 'nprocs %d, i %d, start %d, end %d, peso %d' %(nprocs, i, start, end, peso)
	intPartial = Node(intNum, 3)
	graph.add(intPartial)

	fpeso.add_edge(intPartial,0)
	fstart.add_edge(intPartial,1)
	fend.add_edge(intPartial,2)
	
	intPartial.add_edge(R, i)

sched.start()



